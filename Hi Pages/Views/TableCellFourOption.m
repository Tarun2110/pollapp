//
//  TableCustomCell.m
//  PollingApp
//
//  Created by Rakesh Kumar on 04/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "TableCellFourOption.h"

@implementation TableCellFourOption
@synthesize  DataArray;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.frame = CGRectMake(0, 0, 300, 50);
        UITableView *subMenuTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain]; //create tableview a
        subMenuTableView.backgroundColor = [UIColor redColor];
        subMenuTableView.tag = 100;
        subMenuTableView.delegate = self;
        subMenuTableView.dataSource = self;
        [_answerVieewWM addSubview:subMenuTableView]; // add it cell
       // [subMenuTableView release]; // for without ARC
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    
    _questionViewWM.layer.masksToBounds = NO;
    _questionViewWM.layer.shadowOffset = CGSizeMake(3, 5);
    _questionViewWM.layer.shadowRadius = 3;
    _questionViewWM.layer.shadowOpacity = 0.3;
    
    _centerViewWM.layer.masksToBounds = YES;
    _centerViewWM.layer.cornerRadius = _centerViewWM.frame.size.height/2;
    _centerViewWM.layer.borderWidth = 0.5;
    _centerViewWM.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _answerVieewWM.layer.masksToBounds = NO;
    _answerVieewWM.layer.shadowOffset = CGSizeMake(3, 3);
    _answerVieewWM.layer.shadowRadius = 3;
    _answerVieewWM.layer.shadowOpacity = 0.3;
    
    
    _questionView.layer.masksToBounds = NO;
    _questionView.layer.shadowOffset = CGSizeMake(3, 5);
    _questionView.layer.shadowRadius = 3;
    _questionView.layer.shadowOpacity = 0.3;
    
    _centerView.layer.masksToBounds = YES;
    _centerView.layer.cornerRadius = _centerView.frame.size.height/2;
    _centerView.layer.borderWidth = 0.5;
    _centerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _answerVieew.layer.masksToBounds = NO;
    _answerVieew.layer.shadowOffset = CGSizeMake(3, 3);
    _answerVieew.layer.shadowRadius = 3;
    _answerVieew.layer.shadowOpacity = 0.3;
    
    _Submitbutton.layer.masksToBounds = YES;
    _Submitbutton.layer.cornerRadius = _Submitbutton.frame.size.height/2;
    _Submitbutton.layer.borderWidth = 0.5;
    _Submitbutton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _SubmitbuttonWM.layer.masksToBounds = YES;
    _SubmitbuttonWM.layer.cornerRadius = _SubmitbuttonWM.frame.size.height/2;
    _SubmitbuttonWM.layer.borderWidth = 0.5;
    _SubmitbuttonWM.layer.borderColor = [UIColor lightGrayColor].CGColor;

}




-(void)layoutSubviews
{
    [super layoutSubviews];
    UITableView *subMenuTableView =(UITableView *) [self viewWithTag:100];
    subMenuTableView.frame = CGRectMake(0.2, 0.3, self.bounds.size.width-5,    self.bounds.size.height-5);//set the frames for tableview
    
}

//manage datasource and  delegate for submenu tableview
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableCellFourOption *cell = [tableView dequeueReusableCellWithIdentifier:@"CellId"];
    if(cell == nil)
    {
        cell = [[TableCellFourOption alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellId"];
    }
   
    cell.primaryLabel.text = @"S";
    
    return cell;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
