//
//  TableCustomCell.h
//  PollingApp
//
//  Created by Rakesh Kumar on 04/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCellFourOption : UITableViewCell<UITableViewDelegate,UITableViewDataSource>

@property (strong,nonatomic) NSMutableArray *DataArray;

@property (strong, nonatomic) IBOutlet UIView *questionViewWM;
@property (strong, nonatomic) IBOutlet UIView *answerVieewWM;
@property (strong, nonatomic) IBOutlet UIView *centerViewWM;
@property (strong, nonatomic) IBOutlet UIButton *yesbuttonWM;
@property (strong, nonatomic) IBOutlet UIButton *NoButtonWM;
@property (strong, nonatomic) IBOutlet UILabel *questionLabelWM;
///////
@property (strong, nonatomic) IBOutlet UIView *questionView;
@property (strong, nonatomic) IBOutlet UIView *answerVieew;
@property (strong, nonatomic) IBOutlet UIView *centerView;
@property (strong, nonatomic) IBOutlet UIButton *yesbutton;
@property (strong, nonatomic) IBOutlet UIButton *NoButton;
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;

@property (strong, nonatomic) IBOutlet UIImageView *MediaImage;
@property (weak, nonatomic) IBOutlet UILabel *primaryLabel;
@property (weak, nonatomic) IBOutlet UILabel *optionLabel;
@property (weak, nonatomic) IBOutlet UIButton *SelectedOption;

@property (weak, nonatomic) IBOutlet UIButton *Submitbutton;
@property (weak, nonatomic) IBOutlet UIButton *SubmitbuttonWM;
@property (weak, nonatomic) IBOutlet UIWebView *myWeb;


@property (weak, nonatomic) IBOutlet UILabel *optionOne;
@property (weak, nonatomic) IBOutlet UILabel *OptionTwo;
@property (weak, nonatomic) IBOutlet UILabel *OptionThree;
@property (weak, nonatomic) IBOutlet UILabel *optionFour;

@property (strong, nonatomic) IBOutlet UILabel *optionOneWM;
@property (strong, nonatomic) IBOutlet UILabel *optionTwoWM;
@property (strong, nonatomic) IBOutlet UILabel *optionThreeWM;
@property (strong, nonatomic) IBOutlet UILabel *optionFourWM;


@end
