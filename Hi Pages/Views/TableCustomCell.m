//
//  TableCustomCell.m
//  PollingApp
//
//  Created by Rakesh Kumar on 04/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "TableCustomCell.h"

@implementation TableCustomCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    
    _questionViewWM.layer.masksToBounds = NO;
    _questionViewWM.layer.shadowOffset = CGSizeMake(3, 5);
    _questionViewWM.layer.shadowRadius = 3;
    _questionViewWM.layer.shadowOpacity = 0.3;
    
    _centerViewWM.layer.masksToBounds = YES;
    _centerViewWM.layer.cornerRadius = _centerViewWM.frame.size.height/2;
    _centerViewWM.layer.borderWidth = 0.5;
    _centerViewWM.layer.borderColor = [UIColor lightGrayColor].CGColor;

    _answerVieewWM.layer.masksToBounds = NO;
    _answerVieewWM.layer.shadowOffset = CGSizeMake(3, 3);
    _answerVieewWM.layer.shadowRadius = 3;
    _answerVieewWM.layer.shadowOpacity = 0.3;

    
    _questionView.layer.masksToBounds = NO;
    _questionView.layer.shadowOffset = CGSizeMake(3, 5);
    _questionView.layer.shadowRadius = 3;
    _questionView.layer.shadowOpacity = 0.3;
    
    _centerView.layer.masksToBounds = YES;
    _centerView.layer.cornerRadius = _centerView.frame.size.height/2;
    _centerView.layer.borderWidth = 0.5;
    _centerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _answerVieew.layer.masksToBounds = NO;
    _answerVieew.layer.shadowOffset = CGSizeMake(3, 3);
    _answerVieew.layer.shadowRadius = 3;
    _answerVieew.layer.shadowOpacity = 0.3;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
