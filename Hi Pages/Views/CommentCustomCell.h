//
//  TableCustomCell.h
//  PollingApp
//
//  Created by Rakesh Kumar on 04/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentCustomCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *usernamelabel;
@property (strong, nonatomic) IBOutlet UILabel *timelabel;
@property (strong, nonatomic) IBOutlet UILabel *LikeCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *commentlabel;

@end
