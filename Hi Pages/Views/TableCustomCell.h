//
//  TableCustomCell.h
//  PollingApp
//
//  Created by Rakesh Kumar on 04/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCustomCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *questionViewWM;
@property (strong, nonatomic) IBOutlet UIView *answerVieewWM;
@property (strong, nonatomic) IBOutlet UIView *centerViewWM;
@property (strong, nonatomic) IBOutlet UIButton *yesbuttonWM;
@property (strong, nonatomic) IBOutlet UIButton *NoButtonWM;
@property (strong, nonatomic) IBOutlet UILabel *questionLabelWM;
///////
@property (strong, nonatomic) IBOutlet UIView *questionView;
@property (strong, nonatomic) IBOutlet UIView *answerVieew;
@property (strong, nonatomic) IBOutlet UIView *centerView;
@property (strong, nonatomic) IBOutlet UIButton *yesbutton;
@property (strong, nonatomic) IBOutlet UIButton *NoButton;
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;

@property (strong, nonatomic) IBOutlet UIImageView *mediaImage;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
