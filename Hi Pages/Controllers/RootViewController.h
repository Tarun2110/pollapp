//
//  ViewController.h
//
//  Created by Rakesh Kumar on 19/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface RootViewController : BaseViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (strong, nonatomic) IBOutlet UIView *LoginView;
@property (strong, nonatomic) IBOutlet UIView *SignUpView;
@property (strong, nonatomic) IBOutlet UIButton *ForgotPasswordbutton;

@property (strong, nonatomic) IBOutlet UIButton *LoginViewButton;
@property (strong, nonatomic) IBOutlet UIImageView *SignUpShadowview;
@property (strong, nonatomic) IBOutlet UIImageView *LoginShadowView;
@property (strong, nonatomic) IBOutlet UIButton *SignUpViewButton;

@property (strong, nonatomic) IBOutlet UIImageView *BackgroundView;
@property (strong, nonatomic) IBOutlet UITextField *nameTextfield;
@property (strong, nonatomic) IBOutlet UITextField *emailTetfield;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextfield;

@property (strong, nonatomic) IBOutlet UITextField *loginEmailTextfield;
@property (strong, nonatomic) IBOutlet UITextField *loginPasswordTextfield;


@property (strong, nonatomic) IBOutlet UIButton *maleButton;
@property (strong, nonatomic) IBOutlet UIButton *femaleButton;
@property (strong, nonatomic) IBOutlet UIButton *SignUpButton;
@property (strong, nonatomic) IBOutlet UIButton *LoginInButton;

@end

