//
//  HomeViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 25/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "dataObject.h"

@interface HomeViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>

{
    NSString *selectedCategory;
    NSMutableArray *QuestionArray;
    dataObject *dataToSend;
}
@property (strong, nonatomic) IBOutlet UITableView *DataTable;
@property (strong, nonatomic) IBOutlet UIButton *questionButton;
@property (strong, nonatomic) IBOutlet UIButton *categoryButton;


@end
