//
//  CommentViewController.h
//  PollingApp
//
//  Created by Rakesh Kumar on 05/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "dataObject.h"

@interface CommentViewController : BaseViewController<UIScrollViewDelegate, UIScrollViewAccessibilityDelegate, UITableViewDelegate , UITableViewDataSource,UITextViewDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *myScrollview;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UIView *QuestionView;
@property (strong, nonatomic) IBOutlet UIView *AnswerView;
@property (strong, nonatomic) IBOutlet UIView *CenterView;
@property (strong, nonatomic) IBOutlet UIButton *Goback_Button;
@property (strong, nonatomic) IBOutlet UIButton *SubmitButton;
@property (strong, nonatomic) IBOutlet UITableView *commentTableView;
@property (strong, nonatomic) IBOutlet UIView *pollingView;
@property (strong, nonatomic) IBOutlet UITextView *commentTextview;

@property (strong, nonatomic) IBOutlet UIView *FemalePollView;
@property (strong, nonatomic) IBOutlet UIView *MalePollView;

@property (nonatomic, strong) dataObject * data;



@end
