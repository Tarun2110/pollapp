//  ViewController.m
//  Created by Rakesh Kumar on 19/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.

#import "RootViewController.h"
#import "HomeViewController.h"
#import <QuartzCore/QuartzCore.h>

BOOL SignedUp;
NSString *Gender;
@interface RootViewController ()

@end

@implementation RootViewController

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.APIService checkInternetConnection];
}

-(void)viewWillAppear:(BOOL)animated
{
    [_LoginView setAlpha:0.0f];
    [_SignUpView setAlpha:1.0f];
    [_ForgotPasswordbutton setAlpha:0.0f];
    _BackgroundView.image = [UIImage imageNamed:@"BG_sign_up"];
    SignedUp = TRUE;
}

-(void)viewDidAppear:(BOOL)animated
{
    //Padding View
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_nameTextfield setLeftViewMode:UITextFieldViewModeAlways];
    [_nameTextfield setLeftView:paddingView];
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_emailTetfield setLeftViewMode:UITextFieldViewModeAlways];
    [_emailTetfield setLeftView:paddingView1];
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_passwordTextfield setLeftViewMode:UITextFieldViewModeAlways];
    [_passwordTextfield setLeftView:paddingView2];
    
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_loginEmailTextfield setLeftViewMode:UITextFieldViewModeAlways];
    [_loginEmailTextfield setLeftView:paddingView3];
    
    UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_loginPasswordTextfield setLeftViewMode:UITextFieldViewModeAlways];
    [_loginPasswordTextfield setLeftView:paddingView4];
    
    //View Shadow
    _LoginShadowView.layer.masksToBounds = NO;
    _LoginShadowView.layer.shadowOffset = CGSizeMake(5, 5);
    _LoginShadowView.layer.shadowRadius = 5;
    _LoginShadowView.layer.shadowOpacity = 0.5;
    
    _SignUpShadowview.layer.masksToBounds = NO;
    _SignUpShadowview.layer.shadowOffset = CGSizeMake(5, 5);
    _SignUpShadowview.layer.shadowRadius = 5;
    _SignUpShadowview.layer.shadowOpacity = 0.5;
    
    //Text field Corner
    [_nameTextfield setBorderStyle:UITextBorderStyleNone];
    _nameTextfield.layer.cornerRadius = _nameTextfield.frame.size.height/2;
    _nameTextfield.layer.borderWidth = 0.5;
    _nameTextfield.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _nameTextfield.layer.masksToBounds = YES;
    
    [_emailTetfield setBorderStyle:UITextBorderStyleNone];
    _emailTetfield.layer.cornerRadius = _nameTextfield.frame.size.height/2;
    _emailTetfield.layer.borderWidth = 0.5;
    _emailTetfield.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _emailTetfield.layer.masksToBounds = YES;
    
    [_passwordTextfield setBorderStyle:UITextBorderStyleNone];
    _passwordTextfield.layer.cornerRadius = _nameTextfield.frame.size.height/2;
    _passwordTextfield.layer.borderWidth = 0.5;
    _passwordTextfield.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _passwordTextfield.layer.masksToBounds = YES;
    
    [_loginEmailTextfield setBorderStyle:UITextBorderStyleNone];
    _loginEmailTextfield.layer.cornerRadius = _nameTextfield.frame.size.height/2;
    _loginEmailTextfield.layer.borderWidth = 0.5;
    _loginEmailTextfield.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _loginEmailTextfield.layer.masksToBounds = YES;
    
    [_loginPasswordTextfield setBorderStyle:UITextBorderStyleNone];
    _loginPasswordTextfield.layer.cornerRadius = _nameTextfield.frame.size.height/2;
    _loginPasswordTextfield.layer.borderWidth = 0.5;
    _loginPasswordTextfield.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _loginPasswordTextfield.layer.masksToBounds = YES;
    
    //Button Corners
    _maleButton.layer.borderWidth = 1.0f;
    _maleButton.layer.borderColor = [UIColor colorWithRed:(69/255.0) green:(175/255.0) blue:(240/255.0) alpha:1].CGColor;
    _maleButton.layer.cornerRadius = _maleButton.frame.size.height/2;
    
    _femaleButton.layer.borderWidth = 1.0f;
    _femaleButton.layer.borderColor = [UIColor colorWithRed:(226/255.0) green:(19/255.0) blue:(111/255.0) alpha:1].CGColor;
    _femaleButton.layer.cornerRadius = _maleButton.frame.size.height/2;
}



#pragma mark  Selecting Gender

- (IBAction)TappedAction_SelectedGender:(id)sender
{
    if ([sender tag] == 10)
    {
        Gender = @"m";
        _maleButton.backgroundColor = [UIColor colorWithRed:(69/255.0) green:(175/255.0) blue:(240/255.0) alpha:1] ;
        [_maleButton setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
        
        _femaleButton.backgroundColor = [UIColor whiteColor];
        [_femaleButton setTitleColor:[UIColor colorWithRed:(226/255.0) green:(19/255.0) blue:(111/255.0) alpha:1]forState:UIControlStateNormal];
    }
    else
    {
        Gender = @"f";
        _maleButton.backgroundColor = [UIColor whiteColor];
        [_maleButton setTitleColor:[UIColor colorWithRed:(69/255.0) green:(175/255.0) blue:(240/255.0) alpha:1] forState:UIControlStateNormal];
        
        _femaleButton.backgroundColor = [UIColor colorWithRed:(226/255.0) green:(19/255.0) blue:(111/255.0) alpha:1];
        [_femaleButton setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
    }
}

#pragma mark  Changing Background Image And Views

- (IBAction)TappedAction_SignUp:(id)sender
{
    if (SignedUp == FALSE)
    {
        [UIView transitionWithView:_BackgroundView
                          duration:0.5f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            _BackgroundView.image = [UIImage imageNamed:@"BG_sign_up"];
                        } completion:nil];
        [_LoginView setAlpha:0.0f];
        [_SignUpView setAlpha:1.0f];
        [_ForgotPasswordbutton setAlpha:0.0f];
        SignedUp = TRUE;
    }
}

- (IBAction)TappedAction_Login:(id)sender
{
    if (SignedUp == TRUE)
    {
        [UIView transitionWithView:_BackgroundView
                          duration:0.5f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            _BackgroundView.image = [UIImage imageNamed:@"BG_login"];
                            
                        } completion:nil];
        [_LoginView setAlpha:1.0f];
        [_SignUpView setAlpha:0.0f];
        [_ForgotPasswordbutton setAlpha:1.0f];
        SignedUp = FALSE;
    }
}





#pragma mark  TappedAction For SignIn & SignUp

- (IBAction)TappedAction_LoginButton:(id)sender
{
    if (!validateEmailWithString(_loginEmailTextfield.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Enter valid email." :self];
    }
    else if (_loginPasswordTextfield.text.length <=0)
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please enter your password" :self];
    }
    else
    {
        [self ShowProgress];
        
        NSString* Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
        NSLog(@"output is : %@", Identifier);
        
        [self.APIService API_UserLogin:self withSelector:@selector(outputResultForLogin:) :self.view :_loginEmailTextfield.text :_loginPasswordTextfield.text :@"2" :Identifier];
    }
}


- (IBAction)TappedAction_SignUpButton:(id)sender
{
    if (isStringEmpty(_nameTextfield.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Enter Name." :self];
    }
    else if (!validateEmailWithString(_emailTetfield.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Enter valid email." :self];
    }
    else if (_passwordTextfield.text.length <=0)
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please set your password." :self];
    }
    else if (_passwordTextfield.text.length < 8)
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Password must be eight characters." :self];
    }
    else if (isStringEmpty(Gender))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please select your gender." :self];
    }
    else
    {
        [self ShowProgress];
        [self.APIService API_RegisterUser:self withSelector:@selector(outputforRegister:) :self.view :_nameTextfield.text :_emailTetfield.text :_passwordTextfield.text :Gender :@""];
    }
}




#pragma mark  SignIn & SignUp API Output

- (void) outputResultForLogin:(NSDictionary *)responseDict
{
    [self HideProgress];
    NSString * successString = [responseDict valueForKey:@"success"];
    
    if ([successString isEqualToString:@"1"])
    {
        NSString *token = [[[responseDict valueForKey:@"data"] valueForKey:@"access_token"] objectAtIndex:0];
        [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"TokenId"];
        [self MoveToHome];
    }
    else
    {
        [self.GlobelFucntion alert:@"Alert!" :@"The user does not exist or is not unique." :self];
    }
}

- (void) outputforRegister:(NSDictionary *)responseDict
{
    [self HideProgress];
    NSString * successString = [responseDict valueForKey:@"success"];
    
    if ([successString isEqualToString:@"1"])
    {
        NSString *token = [[responseDict valueForKey:@"data"] valueForKey:@"token"];
        [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"TokenId"];
        [self MoveToHome];
    }
    else
    {
        [self.GlobelFucntion alert:@"Alert!" :@"The user with this email address already exists." :self];
    }
}

-(void)MoveToHome
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LoggedIn"];
    HomeViewController *RootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self.navigationController pushViewController:RootViewController animated:YES];
    
}



#pragma mark  Forgot Button Action and API Output

- (IBAction)TappedAction_forgotButton:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Reset Password!"
                                                                   message:@"Enter the email address you used for sign up."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *submit = [UIAlertAction actionWithTitle:@"Reset" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
    {
                                                if (alert.textFields.count > 0)
                                                {
                                                    UITextField *textField = [alert.textFields firstObject];
                                                   
                                                    [self ShowProgress];

                                                    [self.APIService API_ForgotPassword:self withSelector:@selector(outputforForgot:) :self.view :textField.text];
                                                    
                                                }
                                                       
                                                   }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * action)
                             {
                                                   }];
    [alert addAction:submit];
    [alert addAction:cancel];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField)
    {
    }];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) outputforForgot:(NSDictionary *)responseDict
{
    [self HideProgress];
    NSString * successString = [responseDict valueForKey:@"success"];
    
    if ([successString isEqualToString:@"1"])
    {
        [self.GlobelFucntion alert:@"Success!" :@"We've sent a password reset link to your email address." :self];

    }
    else
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please enter valid email address." :self];
    }
}




-(void)viewWillDisappear:(BOOL)animated
{
    _emailTetfield.text =@"";
    _passwordTextfield.text = @"";
    _loginEmailTextfield.text = @"";
    _nameTextfield.text = @"";
    _loginPasswordTextfield.text = @"";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
