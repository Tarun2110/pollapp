//  HomeViewController.m
//  Created by Rakesh Kumar on 25/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.

#import "HomeViewController.h"
#import "RootViewController.h"
#import "CommentViewController.h"
#import "dataObject.h"
#import "TableCellFourOption.h"
#import "TableCustomCell.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface HomeViewController ()
@end
@implementation HomeViewController
NSString *AnswerID,*QuestionID;

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    QuestionArray = [NSMutableArray new];
    _DataTable.delegate = self;
    _DataTable.dataSource = self;
    
    _questionButton.layer.borderWidth = 1.0f;
    _questionButton.layer.borderColor = [UIColor colorWithRed:(69/255.0) green:(175/255.0) blue:(240/255.0) alpha:1].CGColor;
    _questionButton.layer.cornerRadius = _questionButton.frame.size.height/2;
    
    _categoryButton.layer.borderWidth = 1.0f;
    _categoryButton.layer.borderColor = [UIColor colorWithRed:(69/255.0) green:(175/255.0) blue:(240/255.0) alpha:1].CGColor;
    _categoryButton.layer.cornerRadius = _questionButton.frame.size.height/2;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self ShowProgress];
    selectedCategory = @"1";
    [self.APIService API_GetQuestionByCategory:self withSelector:@selector(outputforAllQuestions:) :self.view :selectedCategory];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


#pragma mark  TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return QuestionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CustomCellText";
    static NSString *mediaCellIdentifier = @"CustomCellMedia";
    
    static NSString *CellIdentifierFourOption= @"FourOption";
    static NSString *mediaCellIdentifierFourOption= @"FourOptionWithMedia";
    
    dataObject *dataValue = [QuestionArray objectAtIndex:indexPath.row];
    
    TableCellFourOption *Cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierFourOption];
    TableCellFourOption *mediaCellFour = [tableView dequeueReusableCellWithIdentifier:mediaCellIdentifierFourOption];
    
    TableCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    TableCustomCell *mediaCell = [tableView dequeueReusableCellWithIdentifier:mediaCellIdentifier];
    
   
    
    if (cell == nil)
    {
        cell = [[TableCustomCell alloc] init];
    }
    if ([dataValue.questiontype isEqualToString:@"1"]) // Category Type
    {
        if ([dataValue.Mediatype isEqualToString:@"0"]) // Text
        {
            cell = [[[NSBundle mainBundle ] loadNibNamed:@"TableCustomCell" owner:self options:nil] objectAtIndex:0];
            cell.questionLabelWM.text = dataValue.questionTitle;
            
            cell.yesbuttonWM.tag = indexPath.row;
            cell.NoButtonWM.tag = indexPath.row;
            
            [cell.yesbuttonWM addTarget:self action:@selector(YesbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.NoButtonWM addTarget:self action:@selector(NobuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
       else
       {
           mediaCell = [[[NSBundle mainBundle ] loadNibNamed:@"TableCustomCell" owner:self options:nil] objectAtIndex:1];
           
           if ([dataValue.Mediatype isEqualToString:@"1"]) // Video
           {
               [mediaCell.mediaImage setHidden:YES];
              
               dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                 
                   NSString *embedHTML = [NSString stringWithFormat:@"<iframe width=\"%f\" height=\"%f\" src=\"%@\" frameborder=\"0\" style=\"margin:-8px;padding:0;\" allowfullscreen></iframe>", mediaCellFour.MediaImage.frame.size.width, mediaCellFour.MediaImage.frame.size.height ,dataValue.questionMedia];
                   
                   [mediaCell.webView loadHTMLString:embedHTML baseURL:nil];
                   
                   [mediaCell.webView.scrollView setScrollEnabled:NO]; // Prevents scrolling in the webview.
                   [mediaCell.webView.scrollView setScrollsToTop:NO];
                   // Update UI
                   dispatch_async(dispatch_get_main_queue(), ^{
                       //weakCell.imageView = image;
                   });
               });
            
           }
           else // Image
           {
               [mediaCell.webView setHidden:YES];
               
               [mediaCell.mediaImage sd_setImageWithURL:[NSURL URLWithString:dataValue.questionMedia]
                                       placeholderImage:[UIImage imageNamed:@"placeholder.png"]];

           }
           mediaCell.questionLabel.text = dataValue.questionTitle;

           mediaCell.yesbutton.tag = indexPath.row;
           mediaCell.NoButton.tag = indexPath.row;
           
           [mediaCell.yesbutton addTarget:self action:@selector(YesbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
           [mediaCell.NoButton addTarget:self action:@selector(NobuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
           
           return mediaCell;
       }
        
    }
    else
    {
        if ([dataValue.Mediatype isEqualToString:@"0"])
        {
            Cell = [[[NSBundle mainBundle ] loadNibNamed:@"TableCellFourOption" owner:self options:nil] objectAtIndex:0];
            Cell.questionLabel.text = dataValue.questionTitle;
            
            Cell.optionOne.text = [NSString stringWithFormat:@"%@",[[dataValue.OptionsArray objectAtIndex:0] valueForKey:@"option"]];
            
            Cell.OptionTwo.text =[NSString stringWithFormat:@"%@",[[dataValue.OptionsArray objectAtIndex:1] valueForKey:@"option"]];
            
            Cell.OptionThree.text = [NSString stringWithFormat:@"%@",[[dataValue.OptionsArray objectAtIndex:2] valueForKey:@"option"]];
            
            Cell.optionFour.text =[NSString stringWithFormat:@"%@",[[dataValue.OptionsArray objectAtIndex:3] valueForKey:@"option"]];
            
            return Cell;
        }
        else
        {
            mediaCellFour = [[[NSBundle mainBundle ] loadNibNamed:@"TableCellFourOption" owner:self options:nil] objectAtIndex:1];
            
            if ([dataValue.Mediatype isEqualToString:@"1"]) // Video
            {
                [mediaCellFour.MediaImage setHidden:YES];
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    
                    NSString *embedHTML = [NSString stringWithFormat:@"<iframe width=\"%f\" height=\"%f\" src=\"%@\" frameborder=\"0\" style=\"margin:-8px;padding:0;\" allowfullscreen></iframe>", mediaCellFour.MediaImage.frame.size.width, mediaCellFour.MediaImage.frame.size.height ,dataValue.questionMedia];
                    
                    [mediaCellFour.myWeb.scrollView setScrollEnabled:NO]; // Prevents scrolling in the webview.
                    [mediaCellFour.myWeb.scrollView setScrollsToTop:NO];
                    [mediaCellFour.myWeb loadHTMLString:embedHTML baseURL:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    });
                });
            }
            else // Image
            {
                [mediaCellFour.MediaImage sd_setImageWithURL:[NSURL URLWithString:dataValue.questionMedia] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
                
                [mediaCellFour.myWeb setHidden:YES];
            }
            mediaCellFour.questionLabelWM.text = dataValue.questionTitle;
            mediaCellFour.optionOneWM.text = [NSString stringWithFormat:@"%@",[[dataValue.OptionsArray objectAtIndex:0] valueForKey:@"option"]];
            
            mediaCellFour.optionTwoWM.text =[NSString stringWithFormat:@"%@",[[dataValue.OptionsArray objectAtIndex:1] valueForKey:@"option"]];
            mediaCellFour.optionThreeWM.text = [NSString stringWithFormat:@"%@",[[dataValue.OptionsArray objectAtIndex:2] valueForKey:@"option"]];
            mediaCellFour.optionFourWM.text =[NSString stringWithFormat:@"%@",[[dataValue.OptionsArray objectAtIndex:3] valueForKey:@"option"]];
            return mediaCellFour;
        }
    }
}


-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    dataObject *dataValue = [QuestionArray objectAtIndex:indexPath.row];
    
    if ([dataValue.questiontype isEqualToString:@"1"])
    {
        if ([dataValue.Mediatype isEqualToString:@"0"])
        {
            return 170;
        }
        else
        {
            return 300;
        }
    }
    else
    {
        if ([dataValue.Mediatype isEqualToString:@"0"])
        {
            return 460;
        }
        else
        {
            return 550;
        }
    }
}


#pragma mark  Button Clicked YES / NO


-(void)YesbuttonClicked:(UIButton*)sender
{
    dataToSend = [QuestionArray objectAtIndex:sender.tag];
    AnswerID = [[dataToSend.OptionsArray objectAtIndex:1] valueForKey:@"id"];
    QuestionID = dataToSend.questionCategoryId ;
    
    [self ShowProgress];
    [self.APIService API_SaveAnswer:self withSelector:@selector(outputForSavingAnswer:) :self.view :QuestionID :AnswerID];
}


-(void)NobuttonClicked:(UIButton*)sender
{
    dataToSend = [QuestionArray objectAtIndex:sender.tag];
    AnswerID = [[dataToSend.OptionsArray objectAtIndex:0] valueForKey:@"id"];
    QuestionID = dataToSend.questionCategoryId ;
    
    
    [self ShowProgress];
    [self.APIService API_SaveAnswer:self withSelector:@selector(outputForSavingAnswer:) :self.view :QuestionID :AnswerID];
}


#pragma mark  BackButton

- (IBAction)TappedAction_Backbutton:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"LoggedIn"];
    RootViewController *RootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"RootViewController"];
    [self.navigationController pushViewController:RootViewController animated:NO];
}

#pragma mark  Output for QuestionAPI

- (void) outputforAllQuestions:(NSDictionary *)responseDict
{
    [QuestionArray removeAllObjects];
    
    [self HideProgress];
    NSString * successString = [responseDict valueForKey:@"success"];
    
    if ([successString isEqualToString:@"1"])
    {
        NSArray *TotalQuestions = [[responseDict valueForKey:@"data"] valueForKey:@"question"];
        
        for (int i = 0; i < TotalQuestions.count; i++)
        {
            dataObject * data = [dataObject new];
            data.questiontype = [[TotalQuestions valueForKey:@"question_type"] objectAtIndex:i];
            data.questionTitle = [[TotalQuestions valueForKey:@"question"] objectAtIndex:i];
            data.Mediatype = [[TotalQuestions valueForKey:@"type"] objectAtIndex:i];
            data.questionMedia = [[TotalQuestions valueForKey:@"media"] objectAtIndex:i];
            data.OptionsArray = [[TotalQuestions valueForKey:@"options"] objectAtIndex:i];
            data.questionCategoryId = [[TotalQuestions valueForKey:@"id"] objectAtIndex:i];
            
            [QuestionArray addObject:data];
        }        
        [_DataTable reloadData];
    }
}

#pragma mark  Output for Saving Answer
- (void) outputForSavingAnswer:(NSDictionary *)responseDict
{
    [self HideProgress];
    NSString * successString = [responseDict valueForKey:@"success"];
    
    if ([successString isEqualToString:@"1"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"pie"];
        
        CommentViewController *RootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CommentViewController"];
       
        RootViewController.data = dataToSend;
        
        
        [self.navigationController pushViewController:RootViewController animated:YES];
    }
}

#pragma mark  Select Category


- (IBAction)TappedAction_SelectedCategory:(id)sender
{
    if ([sender tag] == 1)
    {
        selectedCategory = @"1";
        
        _questionButton.backgroundColor = [UIColor clearColor];
        _questionButton.layer.borderColor = [UIColor colorWithRed:(69/255.0) green:(175/255.0) blue:(240/255.0) alpha:1].CGColor;
        _categoryButton.backgroundColor = [UIColor colorWithRed:(69/255.0) green:(175/255.0) blue:(240/255.0) alpha:1];
        [_questionButton setTitleColor:[UIColor colorWithRed:(69/255.0) green:(175/255.0) blue:(240/255.0) alpha:1] forState:UIControlStateNormal];
        [_categoryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else
    {
        selectedCategory = @"2";
        
        _categoryButton.backgroundColor = [UIColor clearColor];
        _categoryButton.layer.borderColor = [UIColor colorWithRed:(69/255.0) green:(175/255.0) blue:(240/255.0) alpha:1].CGColor;
        _questionButton.backgroundColor = [UIColor colorWithRed:(69/255.0) green:(175/255.0) blue:(240/255.0) alpha:1];
        [_categoryButton setTitleColor:[UIColor colorWithRed:(69/255.0) green:(175/255.0) blue:(240/255.0) alpha:1] forState:UIControlStateNormal];
        [_questionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    [self ShowProgress];
    [self.APIService API_GetQuestionByCategory:self withSelector:@selector(outputforAllQuestions:) :self.view :selectedCategory];
}



-(void)NavigateForCategory2
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"pie"];
    CommentViewController *RootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CommentViewController"];
    [self.navigationController pushViewController:RootViewController animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    sharedCache = nil;
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

- (void)didReceiveMemoryWarning
{
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    sharedCache = nil;
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [super didReceiveMemoryWarning];
}

@end
