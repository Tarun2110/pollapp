//
//  CommentViewController.m
//  PollingApp
//
//  Created by Rakesh Kumar on 05/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "CommentViewController.h"
#import "CommentCustomCell.h"
#import <TWRChart.h>
#import <HACBarChart.h>
#import <HACBarLayer.h>


@interface CommentViewController ()
{
    NSArray *AllCommentsArray;
}
@property(strong, nonatomic) TWRChartView *MalechartView;
@property(strong, nonatomic) TWRChartView *FemalechartView;
@property (weak, nonatomic) IBOutlet HACBarChart *MaleBarchart;
@property (strong, nonatomic) IBOutlet HACBarChart *FemaleBarChart;
@end

@implementation CommentViewController
@synthesize FemaleBarChart,data;

#pragma mark  View Setup
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.commentTextview.delegate = self;
    
    _QuestionView.layer.masksToBounds = NO;
    _QuestionView.layer.shadowOffset = CGSizeMake(3, 5);
    _QuestionView.layer.shadowRadius = 3;
    _QuestionView.layer.shadowOpacity = 0.3;
    
    _CenterView.layer.masksToBounds = YES;
    _CenterView.layer.cornerRadius = _CenterView.frame.size.height/2;
    _CenterView.layer.borderWidth = 0.5;
    _CenterView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _AnswerView.layer.masksToBounds = NO;
    _AnswerView.layer.shadowOffset = CGSizeMake(3, 3);
    _AnswerView.layer.shadowRadius = 3;
    _AnswerView.layer.shadowOpacity = 0.3;
    
    _Goback_Button.layer.masksToBounds = YES;
    _Goback_Button.layer.cornerRadius = _CenterView.frame.size.height/2;
    _Goback_Button.layer.borderWidth = 0.5;
    _Goback_Button.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _SubmitButton.layer.masksToBounds = YES;
    _SubmitButton.layer.cornerRadius = _CenterView.frame.size.height/2;
    _SubmitButton.layer.borderWidth = 0.5;
    _SubmitButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _commentTableView.delegate = self;
    _commentTableView.dataSource = self;
    
    float sizeOfContent = 0;
    NSInteger ht = _contentView.frame.size.height;
    NSInteger ht2 = _commentTableView.frame.size.height;
    
    sizeOfContent = ht + ht2 + 160;
    
    _myScrollview.contentSize = CGSizeMake(_myScrollview.frame.size.width, sizeOfContent);
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
   
    [self ShowProgress];
   /// [self.APIService API_GetPercentage:self withSelector:@selector(OutputGetPercentage:) :self.view :data.questionCategoryId];
    
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    NSLog(@"---- > %@",data.questionTitle);
    
    
    
    [self.APIService API_GetComment:self withSelector:@selector(GetComments:) :self.view :data.questionCategoryId];
}


#pragma mark  TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return AllCommentsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *VideoCellIdentifier = @"CommentCell";
    
    CommentCustomCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:VideoCellIdentifier];
    cell = [[[NSBundle mainBundle ] loadNibNamed:@"CommentCustomCell" owner:self options:nil] objectAtIndex:0];
    
    cell.usernamelabel.text = [[AllCommentsArray valueForKey:@"name"] objectAtIndex:indexPath.row];
   
    cell.commentlabel.text = [[AllCommentsArray valueForKey:@"comment"] objectAtIndex:indexPath.row];
    
    return cell;
}


-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return 105;
}


#pragma mark  Create Pie Chart

-(void)AddMaleChart
{
    _MalechartView = [[TWRChartView alloc] initWithFrame:CGRectMake(0, 0,_MalePollView.frame.size.width, _MalePollView.frame.size.height)];
    _MalechartView.backgroundColor = [UIColor clearColor];
    [self.MalePollView addSubview:_MalechartView];
    [self MaleloadPieChart];
    
}

-(void)AddFemaleChart
{
    _FemalechartView = [[TWRChartView alloc] initWithFrame:CGRectMake(0, 0, _MalePollView.frame.size.width, _FemalePollView.frame.size.height)];
    _FemalechartView.backgroundColor = [UIColor clearColor];
    [self.FemalePollView addSubview:_FemalechartView];
    [self FemaleloadPieChart];
}

- (void)MaleloadPieChart
{
    // Values
    NSArray *values = @[@30, @60];
    
    // Colors
    UIColor *color1 = [UIColor colorWithRed:(69/255.0) green:(175/255.0) blue:(240/255.0) alpha:1];
    UIColor *color2 = [UIColor colorWithRed:66/255.0f green:142/255.0f blue:241/255.0f alpha:1.0];
    NSArray *colors = @[color2, color1];
    
    // Doughnut Chart
    TWRCircularChart *pieChart = [[TWRCircularChart alloc] initWithValues:values
                                                                   colors:colors
                                                                     type:TWRCircularChartTypeDoughnut
                                                                 animated:YES];
    
    // You can even leverage callbacks when chart animation ends!
    [_MalechartView loadCircularChart:pieChart withCompletionHandler:^(BOOL finished) {
        if (finished) {
            NSLog(@"Animation finished!!!");
            [self AddFemaleChart];
        }
    }];
}

- (void)FemaleloadPieChart
{
    // Values
    NSArray *values = @[@80, @20];
    
    // Colors
    UIColor *color1 = [UIColor colorWithRed:(226/255.0) green:(19/255.0) blue:(111/255.0) alpha:1];
    UIColor *color2 = [UIColor colorWithRed:66/255.0f green:142/255.0f blue:241/255.0f alpha:1.0];
    NSArray *colors = @[color1, color2];
    
    // Doughnut Chart
    TWRCircularChart *pieChart = [[TWRCircularChart alloc] initWithValues:values
                                                                   colors:colors
                                                                     type:TWRCircularChartTypeDoughnut
                                                                 animated:YES];
    
    // You can even leverage callbacks when chart animation ends!
    [_FemalechartView loadCircularChart:pieChart withCompletionHandler:^(BOOL finished) {
        if (finished) {
            NSLog(@"Animation finished!!!");
        }
    }];
}

#pragma mark  Create Bar Chart

-(void)MaleBarChartView
{
    _MaleBarchart.typeBar = HACBarType3;
    NSArray *data3 = @[
                       @{kHACPercentage:@20, kHACColor  : [UIColor colorWithRed:66/255.0f green:142/255.0f blue:241/255.0f alpha:1.0], kHACCustomText : @"A"},
                       @{kHACPercentage:@30,  kHACColor  : [UIColor colorWithRed:66/255.0f green:142/255.0f blue:241/255.0f alpha:1.0], kHACCustomText : @"B"},
                       @{kHACPercentage:@10,  kHACColor    : [UIColor colorWithRed:66/255.0f green:142/255.0f blue:241/255.0f alpha:1.0], kHACCustomText : @"C"},
                       @{kHACPercentage:@60,  kHACColor  : [UIColor colorWithRed:66/255.0f green:142/255.0f blue:241/255.0f alpha:1.0], kHACCustomText : @"D"},
                       ];
    
    _MaleBarchart.frame = CGRectMake(10, 20, 120, 120);
    _MaleBarchart.reverse               = NO;     // Margin between bars
    _MaleBarchart.showAxis              = NO;   // Show axis line
    _MaleBarchart.showProgressLabel     = YES;   // Show text for bar
    _MaleBarchart.vertical              = YES;   // Orientation chart
    _MaleBarchart.showDataValue         = YES;   // Show value contains _data, or real percent value
    _MaleBarchart.showCustomText        = NO;   // Show custom text, in _data with key kHACCustomText
    _MaleBarchart.sizeLabelProgress     = 30;    // Width of label progress text
    _MaleBarchart.numberDividersAxisY   = 5;
    _MaleBarchart.animationDuration     = 2;
    _MaleBarchart.progressTextColor     = [UIColor darkGrayColor];
    _MaleBarchart.axisYTextColor        = [UIColor colorWithRed:0.80 green:0.80 blue:0.80 alpha:1.0];
    _MaleBarchart.progressTextFont      = [UIFont fontWithName:@"DINCondensed-Bold" size:6];
    _MaleBarchart.typeBar               = HACBarType2;
    _MaleBarchart.showAxisZeroValue     = NO;
    _MaleBarchart.data                  = data3;
    _MaleBarchart.barsMargin            = 5;
    [_MaleBarchart draw];
    
    [self FemaleBarChartView];
}

-(void)FemaleBarChartView
{
    FemaleBarChart.typeBar = HACBarType3;
    NSArray *data3 = @[
                       @{kHACPercentage:@20, kHACColor  : [UIColor colorWithRed:(226/255.0) green:(19/255.0) blue:(111/255.0) alpha:1], kHACCustomText : @"A"},
                       @{kHACPercentage:@30,  kHACColor  : [UIColor colorWithRed:(226/255.0) green:(19/255.0) blue:(111/255.0) alpha:1], kHACCustomText : @"B"},
                       @{kHACPercentage:@10,  kHACColor    : [UIColor colorWithRed:(226/255.0) green:(19/255.0) blue:(111/255.0) alpha:1], kHACCustomText : @"C"},
                       @{kHACPercentage:@60,  kHACColor  : [UIColor colorWithRed:(226/255.0) green:(19/255.0) blue:(111/255.0) alpha:1], kHACCustomText : @"D"},
                       ];
    
    FemaleBarChart.frame = CGRectMake(10, 20, 120, 120);
    FemaleBarChart.reverse               = NO;     // Margin between bars
    FemaleBarChart.showAxis              = NO;   // Show axis line
    FemaleBarChart.showProgressLabel     = YES;   // Show text for bar
    FemaleBarChart.vertical              = YES;   // Orientation chart
    FemaleBarChart.showDataValue         = YES;   // Show value contains _data, or real percent value
    FemaleBarChart.showCustomText        = NO;   // Show custom text, in _data with key kHACCustomText
    FemaleBarChart.sizeLabelProgress     = 30;    // Width of label progress text
    FemaleBarChart.numberDividersAxisY   = 5;
    FemaleBarChart.animationDuration     = 2;
    FemaleBarChart.progressTextColor     = [UIColor darkGrayColor];
    FemaleBarChart.axisYTextColor        = [UIColor colorWithRed:0.80 green:0.80 blue:0.80 alpha:1.0];
    FemaleBarChart.progressTextFont      = [UIFont fontWithName:@"DINCondensed-Bold" size:6];
    FemaleBarChart.typeBar               = HACBarType2;
    FemaleBarChart.showAxisZeroValue     = NO;
    FemaleBarChart.data                  = data3;
    FemaleBarChart.barsMargin            = 5;
    [FemaleBarChart draw];
}


- (void) GetComments:(NSDictionary *)responseDict
{
    [self HideProgress];
    NSString * successString = [responseDict valueForKey:@"success"];
    
    if ([successString isEqualToString:@"1"])
    {
        AllCommentsArray = [[[responseDict valueForKey:@"data"] valueForKey:@"data"] objectAtIndex:0];
        [_commentTableView reloadData];
    }
}


- (void) OutputGetPercentage:(NSDictionary *)responseDict
{
    [self HideProgress];
    NSString * successString = [responseDict valueForKey:@"success"];
    
    if ([successString isEqualToString:@"1"])
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"pie"])
        {
            [self AddMaleChart];
            [_MaleBarchart setHidden:YES];
            [FemaleBarChart setHidden:YES];
        }
        else
        {
            [self MaleBarChartView];
            [FemaleBarChart setHidden:NO];
            [_MaleBarchart setHidden:NO];
        }
    }
}


- (IBAction)TappedAction_SubmitComment:(id)sender
{
    if ([_commentTextview.text isEqualToString:@"Write Something..."])
    {
        
    }
    else
    {
        [self ShowProgress];
        [self.APIService API_postComment:self withSelector:@selector(OutputtoPostComment:) :self.view :data.questionCategoryId :_commentTextview.text];
    }
}



- (void) OutputtoPostComment:(NSDictionary *)responseDict
{
    [self HideProgress];
    NSString * successString = [responseDict valueForKey:@"success"];
    
    if ([successString isEqualToString:@"1"])
    {
        [_commentTextview setText:@"Write Something..."];
        [_commentTextview resignFirstResponder];
        
       [self.APIService API_GetComment:self withSelector:@selector(GetComments:) :self.view :data.questionCategoryId];
    }
}


#pragma mark  Backbutton

- (IBAction)TappedAction_BackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([_commentTextview.text isEqualToString: @"Write Something..."])
    {
        [_commentTextview setText:@""];
    }
    NSLog(@"did begin editing");
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if ([_commentTextview.text isEqualToString: @""] || (_commentTextview.text.length <= 0))
    {
        [_commentTextview setText:@"Write Something..."];
    }
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([_commentTextview.text isEqualToString:@""])
    {
        NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        NSString *trimmed = [text stringByTrimmingCharactersInSet:whitespace];
        if ([trimmed length] == 0)
        {
            return NO;
        }
    }
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [super didReceiveMemoryWarning];
}

@end
