#import "CallAPI.h"
#import "MPNotificationView.h"
@implementation CallAPI

@synthesize reachability;
@synthesize internetWorking;
@synthesize callBackSelector;
@synthesize callBackTarget;


- (id)init {
    if (self = [super init])
    {
        _GlobelFucntion = [GlobelFunctions new];
    }
    return self;
}

- (void) initAuthorizationFor:(NSString *)methodType
{
}

- (NSString *)setAccessToken
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
}




#pragma mark ********************
#pragma mark  Login API Functions
#pragma mark ********************

- (void)API_UserLogin:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)email :(NSString *)password :(NSString *)device_type :(NSString *)device_id
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        NSDictionary *headers = @{
                                  @"content-type": @"application/json",
                                  };
        NSDictionary *parameters = @{ @"email": email,
                                      @"password": password,
                                      @"device_type": device_type,
                                      @"device_id": device_id };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@login",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}


- (void)API_RegisterUser:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)name :(NSString *)email :(NSString *)password  :(NSString *)gender :(NSString *)profile_pic
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSDictionary *headers =    @{
                                    @"content-type": @"application/json",
                                    };
        NSDictionary *parameters = @{@"name": name,
                                     @"email": email,
                                     @"password": password,
                                     @"gender": gender,
                                     @"profile_pic": profile_pic
                                     };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@register",BaseUrl]]];
        request.timeoutInterval = 25.0;
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}


- (void)API_ForgotPassword:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)email
{
    
    [self checkInternetConnection];

    if (internetWorking == 1)
    {
        NSDictionary *headers = @{
                                  @"content-type": @"application/json",
                                  };
        
        NSDictionary *parameters = @{ @"email": email };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@forgot_password",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

- (void)API_GetQuestionByCategory:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)categoryId
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"TokenId"];
        NSString *authValue = [NSString stringWithFormat:@"Bearer %@", TokenString];
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@question_by_cat_id/%@",BaseUrl,categoryId]]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setAllHTTPHeaderFields:headers];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }

}


- (void)API_SaveAnswer:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)question_id :(NSString *)options_id;
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"TokenId"];
        NSString *authValue = [NSString stringWithFormat:@"Bearer %@", TokenString];
        NSDictionary *headers = @{ @"authorization": authValue,
                                   @"content-type": @"application/json",
                                   };
        NSDictionary *parameters = @{ @"question_id": question_id,
                                      @"options_id": options_id,
                                      };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@save_answer", BaseUrl]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:50.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}


- (void)API_GetComment:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)question_id
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"TokenId"];
        NSString *authValue = [NSString stringWithFormat:@"Bearer %@", TokenString];
        NSDictionary *headers = @{ @"authorization": authValue,
                                   @"content-type": @"application/json",
                                   };
        NSDictionary *parameters = @{ @"question_id": question_id
                                      };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@get_comment?page=1", BaseUrl]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:50.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }

}


- (void)API_GetPercentage:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)question_id
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"TokenId"];
        NSString *authValue = [NSString stringWithFormat:@"Bearer %@", TokenString];
        NSDictionary *headers = @{ @"authorization": authValue,
                                   @"content-type": @"application/json",
                                   };
        NSDictionary *parameters = @{ @"question_id": question_id
                                      };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@get_percentage", BaseUrl]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:50.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}


- (void)API_postComment:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)question_id  :(NSString *)comment
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"TokenId"];
        NSString *authValue = [NSString stringWithFormat:@"Bearer %@", TokenString];
        NSDictionary *headers = @{ @"authorization": authValue,
                                   @"content-type": @"application/json",
                                   };
        NSDictionary *parameters = @{ @"question_id": question_id,
                                      @"comment" : comment
                                      };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@post_comment", BaseUrl]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:50.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
 
}




////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ********************
#pragma mark  Registration API Functions
#pragma mark ********************

- (void)API_RegisterEmailValidation:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)email
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@users/email?email=%@",BaseUrl,email]]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}


#pragma mark ********************
#pragma mark  Project Folders API
#pragma mark ********************

- (void)API_GetProjectFolder:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"TokenId"];
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        NSDictionary *headers = @{
                                  @"authorization": authValue,
                                  };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projectFolders",BaseUrl]]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setAllHTTPHeaderFields:headers];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

- (void)API_GetProjectFoldersById:(NSString *)projectId  forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@/plans",BaseUrl, projectId]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
    
}

#pragma mark ********************
#pragma mark  Get Plans API
#pragma mark ********************


- (void)API_GetPlansForProject:(NSString *)projectId  forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@/plans",BaseUrl,projectId]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}



#pragma mark ********************
#pragma mark  Get Plan Revision
#pragma mark ********************

- (void)API_GetPlanWithPlanId:(NSString *)planId  forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@plans/%@/revision",BaseUrl,planId]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

#pragma mark ********************
#pragma mark  Download video
#pragma mark ********************



-(void)API_DownloadVideo:(NSURL *)url withUUID:(NSString *)UUID forTarget:(id)target and:(NSString *)extension {
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *downloadRequest = [NSMutableURLRequest requestWithURL:url];
        [downloadRequest setAllHTTPHeaderFields:headers];
        
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:downloadRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (error != nil) {
                NSLog(@"Error while download: %@", error.localizedDescription);
                //handling is done on main thread
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[GlobelFunctions alloc] alert:@"Error" :@"There was an error while downloading the plan" :(UIViewController *)target];
                });
            } else {
                NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString  *documentsDirectory = [paths objectAtIndex:0];
                
                NSString  *filePath = [NSString stringWithFormat:@"%@/%@.%@", documentsDirectory,UUID, extension];
                
                
                //saving is done on main thread
                dispatch_async(dispatch_get_main_queue(), ^{
                    [data writeToFile:filePath atomically:YES];
                    
                    NSDictionary *dict = [[NSDictionary alloc] init];
                    dict = @{@"filePath" : filePath, };
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadedFile" object:self userInfo:dict];
                });
            }
        }];
        
        [downloadTask resume];
    }
    
}



#pragma mark ********************
#pragma mark  Get Pin
#pragma mark ********************

- (void)API_GetPinDetailsFOrPinId:(NSString *)pinId  forTarget:(id)target withSelector:(SEL)selector InView:(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@pins/%@",[GlobelFunctions getBaseUrl2],pinId]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}



#pragma mark ********************
#pragma mark  Get Pin Details for a plan
#pragma mark ********************

- (void)API_GetPinDetailsForPlan:(NSString *)planId  forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{
                                  @"authorization": authValue,
                                  };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/plans/%@/pins",BaseUrl,planId]]];
        
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setAllHTTPHeaderFields:headers];
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}


#pragma mark ********************
#pragma mark  Create new Filter
#pragma mark ********************
- (void)API_CreateNewFilters:(NSString *)filters forTarget:(id)target withSelector:(SEL)selector
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        NSError *error;
        NSData *postData = [filters dataUsingEncoding:NSUTF8StringEncoding];
        
        if (error)
        {
            NSLog(@"%@", error.localizedDescription);
        }
        
        NSMutableDictionary *jsonResponse = [[NSMutableDictionary alloc] init];
        jsonResponse = [NSJSONSerialization JSONObjectWithData:postData options:NSJSONReadingAllowFragments error:&error];
        
        if (error)
        {
            NSLog(@"%@", error.localizedDescription);
        }
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@filters",[GlobelFunctions getBaseUrl2]]]];
        
        [request setHTTPMethod:@"POST"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

#pragma mark ********************
#pragma mark  Get All Categories
#pragma mark ********************

- (void)API_GetAllCategories:(NSString *)projectId  forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{
                                  @"authorization": authValue,
                                  };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/projects/%@/categories",[GlobelFunctions getBaseUrl2],projectId]]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setAllHTTPHeaderFields:headers];
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}


#pragma mark ********************
#pragma mark  Get Pin History
#pragma mark ********************

- (void)API_GetPinHistory:(NSString *)pinID forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@issues/%@/history",[GlobelFunctions getBaseUrl2],pinID]]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setAllHTTPHeaderFields:headers];
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}



#pragma mark ********************
#pragma mark  Get Pin HistoryEvent
#pragma mark ********************

- (void)API_GetHistoryEventDetail:(NSString *)pinID forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    
    
    // http://dev.docu.solutions/api/v2/issues/1/history/all?includeDetails=true&until=2017-04-25T12:32:00.048Z
    
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@issues/%@/history/all?includeDetails=true",[GlobelFunctions getBaseUrl2],pinID]]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setAllHTTPHeaderFields:headers];
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}


#pragma mark ********************
#pragma mark  Get project detail
#pragma mark ********************

- (void)API_GetProjectDetail:(NSString *)projectId  forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    NSLog(@"%@",projectId);
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        NSLog(@"%@", TokenString);
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@",BaseUrl,projectId]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}


#pragma mark ********************
#pragma mark  Get project detail
#pragma mark ********************

- (void)API_GetProjectCategoryActive:(NSString *)projectId  forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    NSLog(@"%@",projectId);
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@/categories/active",[GlobelFunctions getBaseUrl2],projectId]]];
        
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

#pragma mark ********************
#pragma mark  To Suggest Tags Functions
#pragma mark ********************


-(void)API_ToSuggestTag:(NSString *)ProjectId forTarget:(id)target withSelector:(SEL)selector :(NSString *)Value :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@/tags?value=%@",[GlobelFunctions getBaseUrl2],ProjectId,Value]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
        
        
    }
}


-(void)API_ToGetPinTitles:(NSString *)ProjectId forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@/pinTitles",[GlobelFunctions getBaseUrl2],ProjectId]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

-(void)API_GetProjectGroup:(NSString *)projectId forTarget:(id)target withSelector:(SEL)selector {
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@/groups/all",[GlobelFunctions getBaseUrl2],projectId]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

#pragma mark ********************
#pragma mark  To Suggest Task Titles
#pragma mark ********************

-(void)API_ToGetTaskTitles:(NSString *)ProjectId forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@/taskTitles",[GlobelFunctions getBaseUrl2],ProjectId]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

#pragma mark ********************
#pragma mark  Create Folder
#pragma mark ********************

#pragma mark ********************
#pragma mark  Create Folder In Project
#pragma mark ********************

-(void)API_CreateFolderInAProject:(NSString *)projectId target:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)name :(NSString *)parentId
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   @"content-type": @"application/json",
                                   };
        
        NSDictionary *parameters;
        if(parentId != nil){
            parameters= @{ @"name": name,
                           @"parentId":parentId
                           };
        }
        else{
            parameters= @{ @"name": name
                           };
        }
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@/planFolders",BaseUrl,projectId]]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
        
    }
}


#pragma mark ********************
#pragma mark  Create Project
#pragma mark ********************

-(void)API_CreateProject:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)name :(NSString *)occurred :(NSString *)parentId
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   @"content-type": @"application/json",
                                   };
        
        
        NSDictionary *parameters = @{ @"name": name,
                                      @"occurred": occurred,
                                      @"parentId": parentId == nil ? @"" : parentId
                                      };
        
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@projects", BaseUrl]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                        
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
        
    }
}


#pragma mark ********************
#pragma mark  Fetch all media posts for pin
#pragma mark ********************



-(void)API_GetAllMediaPostsFor:(NSString *)issueId andMediaFilter:(NSString *)mediaType andStateFilter:(NSString *)state andCreatorFilter:(NSString *)creator andShowInactive:(BOOL)inactive andFrom:(NSString *)from andTo:(NSString *)to andSort:(NSString *)sort andSortDirection:(NSString *)direction forTarget:(id)target withSelector:(SEL)selector andView:(UIView *)targetView{
    
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   @"content-type": @"application/json",
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        NSString *filters = [NSString stringWithFormat:@"filter=%@&sort=%@&sortDirection=%@", mediaType, sort, direction];
        if (![state isEqualToString:@""]) {
            filters = [NSString stringWithFormat:@"%@&stateFilter=%@", filters, state];
        }
        if (![creator isEqualToString:@""]) {
            filters = [NSString stringWithFormat:@"%@&creatorFilter=%@", filters, creator];
        }
        if (inactive) {
            filters = [NSString stringWithFormat:@"%@&showInactive=%@", filters, @(inactive)];
        }
        if (![from isEqualToString:@""]) {
            filters = [NSString stringWithFormat:@"%@&from=%@", filters, from];
        }
        if (![to isEqualToString:@""]) {
            filters = [NSString stringWithFormat:@"%@&to=%@", filters, to];
        }
        
        NSString *urlString = [NSString stringWithFormat:@"%@issues/%@/posts?%@", [GlobelFunctions getBaseUrl2], issueId, filters];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        
        
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
        
    }
    
}


// CREATE A NEW COMMENT FOR POST
#pragma mark ********************
#pragma mark  CREATE A NEW COMMENT FOR POST
#pragma mark ********************
-(void)API_CreateNewCommentForPost:(NSString *)postId andText:(NSString *)text andFiles:(NSArray *)files forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView {
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   @"content-type": @"application/json",
                                   };
        
        
        
        NSDictionary *parameters = @{ @"text": text,
                                      @"attachmentIds": files};
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@posts/%@/comments",[GlobelFunctions getBaseUrl2],postId]]];
        
        
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
        
    }
}


#pragma mark ********************
#pragma mark  Create a new media post
#pragma mark ********************

-(void)API_CreateNewMediaPost:(NSString *)issueId andType:(NSString *)type andDescription:(NSString *)description andFields:(NSArray *)fileIds  forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView {
    
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   @"content-type": @"application/json",
                                   };
        
        NSDictionary *parameters = @{ @"type": type,
                                      @"description": description,
                                      @"fileIds": fileIds};
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@issues/%@/media",[GlobelFunctions getBaseUrl2],issueId]]];
        
        
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
        
    }
}


#pragma mark ********************
#pragma mark  Recent Notes
#pragma mark ********************


-(void)API_GetRecentNotes:(NSString *)projectId forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView {
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@notes/recent?projectId=%@",[GlobelFunctions getBaseUrl2],projectId]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

#pragma mark ********************
#pragma mark  Create Pin
#pragma mark ********************


#pragma mark ********************
#pragma mark  GET ASSIGNEES
#pragma mark ********************



-(void)API_ToGetAllAssignees:(NSString *)ProjectId forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@/assignees/all",BaseUrl,ProjectId]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}




-(void)API_ToGetAllCompanies:(NSString *)ProjectId forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@/companies/all",BaseUrl,ProjectId]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

#pragma mark ********************
#pragma mark  GET ALL PINS FOR PROJECT
#pragma mark ********************



-(void)API_GetAllPinsForProject:(NSString *)ProjectId andFilterId:(NSString *)filterId andFilter:(NSString *)filter forTarget:(id)target withSelector:(SEL)selector InView:(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        NSLog(@"%@", TokenString);
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@/pins?page=0&size=50&sort=Title&direction=DESC&filter=%@&filterId=%@",[GlobelFunctions getBaseUrl2],ProjectId,filter, filterId]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

-(void)API_AddPinsToGroup:(NSString *)groupId andPins:(NSArray *)pins forTarget:(id)target withSelector:(SEL)selector InView:(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        NSLog(@"%@", TokenString);
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@groups/%@/pins",[GlobelFunctions getBaseUrl2],groupId]]];
        
        [request setHTTPMethod:@"POST"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:pins options:0 error:nil];
        
        [request setHTTPBody:postData];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
    
}

#pragma mark ********************
#pragma mark  GET DEPENDENCIES FOR PIN
#pragma mark ********************



-(void)API_ToGetDependenciesInPin:(NSString *)PinId forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@issues/%@/dependencySuggestions",[GlobelFunctions getBaseUrl2],PinId]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

#pragma mark ********************
#pragma mark  GET DEPENDENCIES FOR PROJECT
#pragma mark ********************



-(void)API_ToGetDependenciesInProject:(NSString *)projectId forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@/dependencySuggestions",[GlobelFunctions getBaseUrl2],projectId]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}



#pragma mark ********************
#pragma mark  IMPORT A MAP PLAN
#pragma mark ********************




-(void)API_ImportMapPlan:(NSString *)mapInfo andProjectId:(NSString *)projectId forTarget:(id)target withSelector:(SEL)selector inView:(UIView *)targetView {
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   @"content-type": @"application/json",
                                   };
        //NSData *postData = [NSJSONSerialization dataWithJSONObject:mapInfo options:0 error:nil];
        
        NSData *data = [mapInfo dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@/geoImport",[GlobelFunctions getBaseUrl2],projectId]]];
        
        
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:data];
        
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
        
    }
}


-(void)API_ListCurrentImport:(id)target withSelector:(SEL) selector inView:(UIView *)targetView {
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   @"content-type": @"application/json",
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@planImports/my",[GlobelFunctions getBaseUrl2]]]];
        
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
        
    }
}


-(void)API_CancelImportById:(NSString *)planId forTarger:(id)target withSelector:(SEL) selector inView:(UIView *)targetView {
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   @"content-type": @"application/json",
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@planImports/jobs/%@",[GlobelFunctions getBaseUrl2], planId]]];
        
        
        
        [request setHTTPMethod:@"DELETE"];
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
        
    }
}


-(void)API_CancelAllImport:(id)target withSelector:(SEL) selector inView:(UIView *)targetView {
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   @"content-type": @"application/json",
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@planImports/jobs/",[GlobelFunctions getBaseUrl2]]]];
        
        
        
        [request setHTTPMethod:@"DELETE"];
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
        
    }
}




#pragma mark ********************
#pragma mark  GET ALL TASKS FOR PIN
#pragma mark ********************


-(void)API_GetAllTasksForProject:(NSString *)projectId andFilterId:(NSString *)filterId andPage:(int)page andSize:(int)size andSort:(NSString *)sort andDirection:(NSString *)direction andFilter:(NSString*)filter forTarget:(id)target withSelector:(SEL)selector InView:(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@/tasks?page=%i&size=%i&sort=%@&direction=%@&filter=%@&filterId=%@",[GlobelFunctions getBaseUrl2],projectId, page, size, sort, direction, filter, filterId]]];
        
        
        NSLog(@"%@", [request URL]);
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}


-(void)API_GetAllTasksForPin:(NSString *)pinId forTarget:(id)target withSelector:(SEL)selector InView:(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@issues/%@/posts?filter=Task",[GlobelFunctions getBaseUrl2],pinId]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}



#pragma mark *********************
#pragma mark GET ALL GROUPS
#pragma mark *********************
-(void)API_GetAllGroupsForProject:(NSString *)projectId search:(NSString *)search showInactive:(BOOL)showInactive type:(NSString *)type andSort:(NSString *)sort andsortDirection:(NSString *)sortDirection quickFilter:(NSString *)quickFilter andPage:(int)page asdSize:(int)size forTarget:(id)target withSelector:(SEL)selector InView:(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@/groups?search=%@&showInactive=%i&type=%@&sort=%@&sortDirection=%@&page=%i&size=%i&quickFilter=%@",[GlobelFunctions getBaseUrl2],projectId, search, showInactive, type, sort, sortDirection, page, size,quickFilter]]];
        
        
        NSLog(@"%@", [request URL]);
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

-(void)API_CreateGroupForProject:(NSString *)projectId group:(NSString *)group forTarget:(id)target withSelector:(SEL)selector InView:(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@projects/%@/groups",[GlobelFunctions getBaseUrl2],projectId]]];
        
        
        NSLog(@"%@", [request URL]);
        
        [request setHTTPMethod:@"POST"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSData *postData = [group dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}
-(void)API_UpdateGroups:(NSArray *)groups forTarget:(id)target withSelector:(SEL)selector InView:(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@groups",[GlobelFunctions getBaseUrl2]]]];
        
        
        NSLog(@"%@", [request URL]);
        
        [request setHTTPMethod:@"PATCH"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:groups options:0 error:nil];
        
        [request setHTTPBody:postData];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}
-(void)API_GetAllPinsForGroup:(NSString *)groupId andFilter:(NSString *)filter forTarget:(id)target withSelector:(SEL)selector InView:(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@groups/%@/pins/all?sort=Title&direction=DESC&filter=%@",[GlobelFunctions getBaseUrl2],groupId,filter]]];
        
        
        NSLog(@"%@", [request URL]);
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
    
}

-(void)API_CreateDuplicateGroup:(NSString *)groupId group:(NSDictionary *)group forTarget:(id)target withSelector:(SEL)selector InView:(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@groups/%@/duplicate",[GlobelFunctions getBaseUrl2],groupId]]];
        
        
        NSLog(@"%@", [request URL]);
        
        [request setHTTPMethod:@"POST"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:group options:0 error:nil];
        
        [request setHTTPBody:postData];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

-(void)API_RemovePinsFromGroup:(NSString *)groupId pins:(NSArray *)pins forTarget:(id)target withSelector:(SEL)selector InView:(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@groups/%@/pins",[GlobelFunctions getBaseUrl2],groupId]]];
        
        
        NSLog(@"%@", [request URL]);
        
        [request setHTTPMethod:@"DELETE"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:pins options:0 error:nil];
        
        [request setHTTPBody:postData];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

#pragma mark ********************
#pragma mark  UPDATE MEDIA POST
#pragma mark ********************

-(void)API_UpdateMediaPost:(NSString *)postId andNewDescription:(NSString *)description forTarget:(id)target withSelector:(SEL)selector {
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   @"content-type": @"application/json",
                                   };
        
        
        NSDictionary *taskSubstatus = @{@"description" : description};
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:taskSubstatus options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@media/%@",[GlobelFunctions getBaseUrl2],postId]]];
        [request setHTTPMethod:@"PATCH"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

#pragma mark ********************
#pragma mark  GET COMMENTS FROM A POST
#pragma mark ********************


-(void)API_GetAllComentsFromPost:(NSString *)postId forTarget:(id)target withSelector:(SEL)selector {
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@posts/%@/comments",[GlobelFunctions getBaseUrl2],postId]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

#pragma mark ********************
#pragma mark  GET USER DETAIL
#pragma mark ********************


-(void)API_GetUserDetailforTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        
        NSDictionary *headers = @{ @"authorization": authValue,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@me",BaseUrl]]];
        
        [request setHTTPMethod:@"GET"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}


//To Get SearchData

-(void)SearchAPI_GetPins:(NSString *)projectId searchText:(NSString *)searchText forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        NSDictionary *headers = @{
                                  @"authorization": authValue,
                                  };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@pins/search?search=%@for=%@",[GlobelFunctions getBaseUrl2],searchText,projectId]]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setAllHTTPHeaderFields:headers];
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

-(void)SearchAPI_GetTasks:(NSString *)projectId searchText:(NSString *)searchText forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        NSDictionary *headers = @{
                                  @"authorization": authValue,
                                  };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@tasks/search?search=%@&for=%@",[GlobelFunctions getBaseUrl2],searchText,projectId]]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setAllHTTPHeaderFields:headers];
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

-(void)SearchAPI_GetPlans:(NSString *)projectId searchText:(NSString *)searchText forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        NSDictionary *headers = @{
                                  @"authorization": authValue,
                                  };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@plans/search?search=%@&for=%@",BaseUrl,searchText,projectId]]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setAllHTTPHeaderFields:headers];
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

-(void)SearchAPI_GetTeamMembers:(NSString *)projectId searchText:(NSString *)searchText forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        NSDictionary *headers = @{
                                  @"authorization": authValue,
                                  };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@team/search?search=%@&for=%@",BaseUrl,searchText,projectId]]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setAllHTTPHeaderFields:headers];
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

-(void)SearchAPI_GetGroups:(NSString *)projectId searchText:(NSString *)searchText forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        NSDictionary *headers = @{
                                  @"authorization": authValue,
                                  };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@groups/search?search=%@&for=%@",[GlobelFunctions getBaseUrl2],searchText,projectId]]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setAllHTTPHeaderFields:headers];
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}

-(void)SearchAPI_GetDataSets:(NSString *)projectId searchText:(NSString *)searchText forTarget:(id)target withSelector:(SEL)selector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSString*  TokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
        NSString *authValue = [NSString stringWithFormat:@"bearer %@", TokenString];
        NSDictionary *headers = @{
                                  @"authorization": authValue,
                                  };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@datasets/search?search=%@&for=%@",BaseUrl,searchText,projectId]]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setAllHTTPHeaderFields:headers];
        [self handleOutputForOther:request withTarget:target withSelector:selector];
    }
}



//#pragma mark **********************
//#pragma mark - Output Section
//#pragma mark **********************

- (void)handleOutputForOther:(NSMutableURLRequest *)urlRequest withTarget:(id)tempTarget withSelector:(SEL)tempSelector
{
    NSURLSession *session = [NSURLSession sharedSession];
    NSDictionary *dict;
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error)
                                          {
                                              NSLog(@"%@", error);
                                          }
                                          else
                                          {
                                              NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                              
                                              NSMutableDictionary *jsonResponse = [[NSMutableDictionary alloc] init];
                                              //  [jsonResponse setObject:[NSNumber numberWithInteger:[httpResponse statusCode]] forKey:@"statusCode"];
                                              
                                              jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments                                                                                                            error:&error];
                                              
                                              //                                              if (error)
                                              //                                              {
                                              //                                                  NSLog(@"there was an error while parsing data JSON: %@", [error localizedDescription]);
                                              //                                              }
                                              //
                                              //                                              NSLog(@"%@",jsonResponse);
                                              //
                                              //                                              NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                              //
                                              
                                              
                                              
                                              [self showOutPutHereWithTarget:tempTarget withSelector:tempSelector withDictionary:jsonResponse];
                                              
                                          }
                                      }];
    [dataTask resume];
}



- (void)showOutPutHereWithTarget:(id)tempTarget withSelector:(SEL)stateSelector  withDictionary:(NSDictionary *)dict
{
    [tempTarget performSelectorOnMainThread:stateSelector withObject:dict waitUntilDone:YES];
}

#pragma mark **********************
#pragma mark - Reachability Methods
#pragma mark **********************

- (void)checkInternetConnection
{
    reachability = [Reachability reachabilityForInternetConnection];
    [self performSelector:@selector(updateInterfaceWithReachability:)withObject:reachability];
}

- (void)updateInterfaceWithReachability:(Reachability *)curReach
{
    if(curReach == reachability)
    {
        NetworkStatus netStatus = [curReach currentReachabilityStatus];
        switch (netStatus)
        {
            case NotReachable:
            {
                internetWorking = 0;
                [MPNotificationView notifyWithText:@"No Network Connection"
                                            detail:@"Connect to a Wi-Fi network or a cellular data."
                                             image:nil
                                          duration:2.0
                                              type:@"Custom"
                                     andTouchBlock:^(MPNotificationView *notificationView)
                 {
                     NSLog( @"Received touch for notification with text: %@", notificationView.textLabel.text );
                 }];
                //  HideProgressHUD;
                break;
            }
            case ReachableViaWiFi:
            {
                internetWorking = 1;
                break;
            }
            case ReachableViaWWAN:
            {
                internetWorking = 1;
                break;
            }
        }
    }
}


-(void)noNetworkAlert
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowServerError" object:nil];
}

@end
