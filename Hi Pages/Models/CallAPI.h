#import <Foundation/Foundation.h>
#import "GlobelFunctions.h"
#import "Reachability.h"

@class BaseViewController;


@interface CallAPI : NSObject<NSXMLParserDelegate>
{
    __weak NSString *deviceName;
    
    NSURLConnection *theConnection;
    NSMutableData   *mutResponseData;
    int             intResponseCode;
    Reachability    *reachability;
    NSString *str_currentElement;
    SEL callBackSelector;
    id __weak callBackTarget;
}


@property (strong ,nonatomic) BaseViewController *objBaseVC;
@property (strong,nonatomic) GlobelFunctions *GlobelFucntion;
@property (nonatomic, strong) Reachability *reachability;
@property (nonatomic, readwrite) NSInteger internetWorking;
@property (nonatomic) SEL callBackSelector;
@property (nonatomic, weak) id callBackTarget;

- (void)checkInternetConnection;

//API FOR USER AUTHENTICATION//



- (void)API_RegisterUser:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)name :(NSString *)email :(NSString *)password  :(NSString *)gender :(NSString *)profile_pic;

- (void)API_UserLogin:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)email :(NSString *)password :(NSString *)device_type :(NSString *)device_id;

- (void)API_ForgotPassword:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)email ;
    
    
- (void)API_GetQuestionByCategory:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)categoryId;

- (void)API_SaveAnswer:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)question_id :(NSString *)options_id;


- (void)API_GetComment:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)question_id;

- (void)API_GetPercentage:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)question_id;

- (void)API_postComment:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)question_id  :(NSString *)comment;



@end
