//
//  dataObject.h
//  PollingApp
//
//  Created by Rakesh Kumar on 09/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface dataObject : NSObject

@property (nonatomic, strong) NSString * questionTitle, *questionCategoryId, *questionMedia;
@property (nonatomic, strong) NSString * questiontype; // 1 for YesOrNO , 2 for Multiple
@property (nonatomic, strong) NSString * Mediatype; // 0 for Text , 1 for video , 2 for Image
@property (nonatomic, strong) NSMutableArray * OptionsArray;

@end
