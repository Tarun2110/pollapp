



#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import <sys/utsname.h>
#import <MessageUI/MessageUI.h>




@interface GlobelFunctions : NSObject<MFMailComposeViewControllerDelegate>

#define BaseUrl @"http://pollapp.seraphic.tech/api/"

@property (nonatomic, strong) UIToolbar* toolbar;

BOOL isStringEmpty(NSString *string);
BOOL isStringEqualZero(NSString *string);
BOOL validateEmailWithString(NSString *emailText);

- (NSString *)getDeviceModelName;
- (NSString *)getCurrentTime;
+ (NSString *)getBaseUrl2;
- (NSString *)getMimeTypeFunction:(NSString *)mimeType;
- (void)makeCallToNumber:(NSString *)phoneNumber;
- (void)showLocationInAppleMap:(NSString *)location;

- (NSString *)changeDateString:(NSString *)dateString;
- (NSString *)changeDateStringWithMiliSeconds:(NSString *)dateString;
- (NSString *)changeDateStringWithMiliMiliSeconds:(NSString *)dateString;
- (NSString *)changeDateStringWithSeconds:(NSString *)dateString;

- (void)alert: (NSString *)alertMsg :(NSString *)message :(UIViewController *)TargetController;
- (void)addBaseLineBelowTextField :(NSString *)placeholder textfield:(UITextField *)textfield;
- (void)openMailComposer :(UIViewController *)TargetController;

@end


